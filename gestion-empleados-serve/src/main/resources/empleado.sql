--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10 (Ubuntu 10.10-1.pgdg18.04+1)
-- Dumped by pg_dump version 10.10 (Ubuntu 10.10-1.pgdg18.04+1)

-- Started on 2019-11-24 01:26:41 +09

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 505 (class 1247 OID 16409)
-- Name: genero; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.genero AS ENUM (
    'M',
    'F'
);


ALTER TYPE public.genero OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 16418)
-- Name: empleados; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.empleados (
    id_empleado integer NOT NULL,
    nombres character varying(25) NOT NULL,
    apellido_paterno character varying(16) NOT NULL,
    apellido_materno character varying(16) NOT NULL,
    genero public.genero,
    fecha_nacimiento date NOT NULL,
    fecha_registro date NOT NULL,
    telefono integer
);


ALTER TABLE public.empleados OWNER TO postgres;

--
-- TOC entry 2950 (class 0 OID 16418)
-- Dependencies: 196
-- Data for Name: empleados; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.empleados (id_empleado, nombres, apellido_paterno, apellido_materno, genero, fecha_nacimiento, fecha_registro, telefono) FROM stdin;
1	Naaman Alejandro	Falcon	Olguin	M	1993-01-05	2019-11-22	935029635
2	Benjamin s	Falcon	Olguin	M	2000-08-07	2019-11-22	935029635
3	Gabriela Fernanda	Villablanca	Merino	F	1999-08-26	2019-11-22	999369013
4	ahmad solehin	Perez	Perez	F	1993-01-05	2019-11-22	935029635
\.


--
-- TOC entry 2828 (class 2606 OID 16422)
-- Name: empleados empleados_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empleados
    ADD CONSTRAINT empleados_pkey PRIMARY KEY (id_empleado);


-- Completed on 2019-11-24 01:26:41 +09

--
-- PostgreSQL database dump complete
--

