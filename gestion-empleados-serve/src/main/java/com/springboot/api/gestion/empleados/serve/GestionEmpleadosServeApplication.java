package com.springboot.api.gestion.empleados.serve;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionEmpleadosServeApplication {
	public static void main(String[] args) {
		SpringApplication.run(GestionEmpleadosServeApplication.class, args);
	}

}
