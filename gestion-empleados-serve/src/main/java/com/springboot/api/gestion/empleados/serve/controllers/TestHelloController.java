package com.springboot.api.gestion.empleados.serve.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("rest/hello")
public class TestHelloController {

    @GetMapping
    public String hello (){
        return "test swagger heelo controller!!";
    }

    @PostMapping("/post")
    public String helloGet(@RequestBody final String hello){
        return hello;
    }

    @PostMapping("/put")
    public String helloPut(@RequestBody final String hello){
        return hello;
    }


}
