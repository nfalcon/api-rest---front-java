package com.springboot.api.gestion.empleados.serve.modelo;

public enum Genero {
    M("Masculino"),
    F("Femenino");

    private final String texto;

    Genero(String texto){
        this.texto= texto;
    }
    public String getTexo(){
        return texto;
    }
}
