package com.springboot.api.gestion.empleados.serve;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.util.Properties;

@Configuration
public class GestionEmpleadosServeConfig {

    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource bundle = new ReloadableResourceBundleMessageSource();
        bundle.setDefaultEncoding("UTF-8");
        Properties fileEncodings = new Properties();
        bundle.setFileEncodings(fileEncodings);
        bundle.setFallbackToSystemLocale(true);
        bundle.setBasename("classpath:application-gestion-empleados-serve-messages");
        return bundle;
    }
}
