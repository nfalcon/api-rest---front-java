package com.springboot.api.gestion.empleados.serve;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.MediaType;

import com.google.common.base.Predicates;
import com.google.common.collect.Sets;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.TagsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import java.util.Locale;

@Configuration
@EnableSwagger2
public class GestionEmpleadosServeSwagger2Configuration {

    @Bean
    public Docket loginApi(ReloadableResourceBundleMessageSource messageSource) {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("gestion-empleados-serve")
                .apiInfo(apiInfo(messageSource))
                .forCodeGeneration(true).select()
                .apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework"))).build()
                .produces(Sets.newHashSet(MediaType.APPLICATION_JSON_VALUE))
                .consumes(Sets.newHashSet(MediaType.APPLICATION_JSON_VALUE))
                .enableUrlTemplating(false)
                .useDefaultResponseMessages(false);
    }

    private ApiInfo apiInfo(ReloadableResourceBundleMessageSource messageSource) {
            return new ApiInfoBuilder()
                    .title(messageSource.getMessage("gestion-empleados-serve.titulo", null, Locale.getDefault()))
                    .description("gestion-empleados-serve").build();
        }
    @Bean
    public UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder()
                .displayOperationId(true)
                .defaultModelsExpandDepth(1)
                .defaultModelExpandDepth(1)
                .docExpansion(DocExpansion.NONE)
                .filter(true)
                .displayRequestDuration(true)
                .maxDisplayedTags(null)
                .operationsSorter(OperationsSorter.METHOD)
                .showExtensions(false)
                .tagsSorter(TagsSorter.ALPHA)
                .supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS).validatorUrl(null)
                .build();
    }


}
